section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax

    .loop:
    cmp byte [rdi + rax], 0
    je .end
    inc rax
    jmp .loop

    .end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi, rdi
    
    push rsi
    call string_length
    pop rsi

    test rax, rax
    je .end

    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    .end:
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi

    mov rsi, rsp
    mov rax, 1
    mov rdi, 1
    mov rdx, 1

    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 10
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    dec rsp
    mov byte[rsp], 0

    xor rcx, rcx
    mov r9, 10

    .loop:
    xor rdx, rdx
    div r9
    add rdx, '0'
    dec rsp
    mov [rsp], dl
    inc rcx

    test rax, rax
    jne .loop

    mov rdi, rsp    
    push rcx
    call print_string
    pop rcx
    inc rsp
    add rsp, rcx

    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns .end

    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint

    .end:
        call print_uint
        ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx

    .loop:
    mov dl, byte [rdi + rcx]
    cmp byte [rsi + rcx], dl
    jne .not_equal
    test dl, dl
    jz .equal
    inc rcx
    jmp .loop

    .equal:
    mov rax, 1
    ret
    .not_equal:
    mov rax, 0
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    xor rax, rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall

    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rcx,rcx
    .loop:
        cmp rcx, rsi
        jge .error

        push rdi
        push rsi
        push rcx
        call read_char
        pop rcx
        pop rsi
        pop rdi

        cmp al,0x20
        je .skip
        cmp al,0x9
        je .skip
        cmp al,0xA
        je .skip
        
        mov byte[rdi+rcx],al
        test al,al
        je .end
        inc rcx
        jmp .loop
        
    .skip:
        test rcx,rcx
        jz .loop
    .end:
        mov rax, rdi
        mov rdx, rcx
        ret
    .error:
        xor rax, rax
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    xor rdx, rdx

    mov r10, 10
    .loop:
    cmp byte[rdi + rcx], '0'
    jl .end
    cmp byte[rdi + rcx], '9'
    jg .end
    
    mul r10
    mov dl, [rdi + rcx]
    sub rdx, '0'
    add rax, rdx
    
    inc rcx

    jmp .loop

    .end:
    mov rdx, rcx
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    je .negative

    jmp parse_uint

    .negative:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret
    

    

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax

    .loop:
        cmp rdx, rax
        je .error

        mov r10b, [rdi + rax]
        mov [rsi + rax], r10b
        inc rax    

        test r10b,r10b
        je .end
        jmp .loop
    .error:
        xor rax, rax
    .end:
        ret